<?php
function Replace_Index($html,$web_name,$web_title,$web_header_title){
    $html = str_replace("{YYZI:web_name}","{$web_name}","{$html}");
    $html = str_replace("{YYZI:web_title}",",{$web_title}","{$html}");
    $html = str_replace("{YYZI:web_header_title}","{$web_header_title}","{$html}");
    return($html);
}
function Replace_Pages($html,$pages,$author){
    $html = str_replace('{YYZI:foreach id=1}',"","{$html}");
    $html = str_replace('{YYZI:/foreach}',"","{$html}");
    $html = str_replace('{YYZI:$pages}',"{$pages}","{$html}");
    $html = str_replace('{YYZI:$author}',"{$author}","{$html}");
    return($html);
}
function Get_Pages_Panel($html){
    $start = stripos("{$html}","{YYZI:foreach id=1}");
    $end = stripos("{$html}","{YYZI:/foreach}") - "{$start}";
    $html = substr("{$html}","{$start}","{$end}");
    return($html);
}
function Get_Index_Footer($html){
    $start = stripos("{$html}","{YYZI:/foreach}") + strlen("{YYZI:/foreach}");
    $end = strlen("{$html}")-stripos("{$html}","{YYZI:/foreach}") + strlen("{YYZI:/foreach}");
    $html = substr("{$html}","{$start}","{$end}");
    return($html);
}
?>