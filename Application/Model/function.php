<?php
// 公共函数列表



//覆盖句子到文件
function WritePages($file_name,$pages){
    $file = fopen($file_name,"w");
    fwrite($file,$pages);
    fclose($file);
}

//随机输出文件中存储的句子
function RandPages($file_name){
    return(RandChars(TurnChars(GetAll($file_name))));
}

// 文本转为字符串
function TurnChars($text){
    return(explode("\n",$text));
}

//随机输出数组中的任意一项
function RandChars($chars){
    return($chars[rand(0,(count($chars)-1))]);
}

//读取文件所有内容
function GetAll($file_name){
    $file = fopen($file_name,"r");
    $result = '';
        while(1){
            //feof() 函数检测是否已到达文件末尾
            if(feof($file)){return($result);}
            else{$result = $result.fgets($file);}
        }
    fclose($file);
}

?>