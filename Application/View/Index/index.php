<?php
// 视图解析

// 加载函数列表
include("Application/View/Index/function.php");
//仅测试用（写入cookies，模拟用户登录
setcookie("user", "root", time()+3600);

// 加载模板文件目录
$filename_index = __YYZI_THEME_DIR__ ;


// 加载网页标题
$web_name = __YYZI_WEB_NAME__ ;
$web_title = __YYZI_WEB_TITLE__ ;
$web_header_title = __YYZI_HEADER_TITLE__ ;

// 加载模板
$index_html = Replace_Index(GetAll($filename_index."/index/index.html"),$web_name,$web_title,$web_header_title);
$panel_html = Get_Pages_Panel($index_html);
$index_header = substr($index_html,0,stripos($index_html,$panel_html));
$index_footer = Get_Index_Footer($index_html);

//输出页面头部
echo $index_header;

// 连接到数据库
include("Application/Model/DB/connect.php");
// 执行SQL语句
$sql = "SELECT * FROM `pages` ORDER BY `id` DESC";
$sth = $pdo->prepare($sql);
$sth->execute();
$data = $sth->fetchAll();
// var_dump($data[0]["page"]);
// var_dump($data[0]["author"]);
foreach ($data as $v){
    $page = $v["page"];
    $author = $v["author"];
    echo Replace_Pages($panel_html,$page,$author);
}
// $sql = "SELECT page FROM pages ORDER BY RAND() LIMIT 1";
// include("Application/Model/DB/sql.php"); 
// var_dump($result);
// // 输出页面底部
// echo $index_footer;
?>