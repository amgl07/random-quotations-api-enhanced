<?php
// 视图解析


// 加载函数列表
include("Application/View/Index/function.php");

// 加载模板文件目录
$filename_index = __YYZI_THEME_DIR__ ;

// 加载网页标题
$web_name = __YYZI_WEB_NAME__ ;

// 加载模板
$html = GetAll($filename_index."/index/login.html");

// 替换模板内的内容
$login_html = str_replace("{YYZI:web_name}","{$web_name}","{$html}");

// 输出登录页面
echo $login_html;
?>